const express = require('express')
const router = express.Router()
const create = require('../controllers/employee/create')
const findMany = require('../controllers/employee/findMany')
const findOne = require('../controllers/employee/findOne')
const remove = require('../controllers/employee/remove')
const update = require('../controllers/employee/update')

router.get('/', findMany)
router.get('/:id', findOne)
router.post('/', create)
router.patch('/:id', update)
router.delete('/:id', remove)

module.exports = router