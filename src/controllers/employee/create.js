const TraceError = require('trace-error')
const datasource = require('../../services/datasource')

const create = async (req, res, next) => {
  try {
    res.send(await datasource.create('employee', req.body, { type: true, shop: true }))
  } catch (error) {
    next(new TraceError('employee:create', error))
  }
}

module.exports = create