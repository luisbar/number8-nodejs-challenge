const TraceError = require('trace-error')
const datasource = require('../../services/datasource')

const findOne = async (req, res, next) => {
  try {
    res.send(await datasource.findOne('employee', { id: parseInt(req.params.id) }, { type: true, shop: true }))
  } catch (error) {
    next(new TraceError('employee:findOne', error))
  }
}

module.exports = findOne