const express = require('express')
const app = express()
const employeeRoutes = require('./src/routes/employee')

app.use(express.json())
app.use('/employee', employeeRoutes)
app.use((error, req, res, next) => {
  console.log(error);
  res.send(error.messages().slice(-1).pop())
})

app.listen(process.env.PORT || 30001, () => { console.log(`Server running on ${process.env.PORT || 30001}`) })