## How to run

1. Install/Use node version
```bash
nvm install && nvm use
```

2. Install dependences
```bash
npm i
```

3. Run MySql container using docker
```bash
docker run -d -p 3306:33306 -e MYSQL_ROOT_PASSWORD=123456 mysql
```

4. Run the project
```bash
npm start
```

## Update your database
if you want to update your database you have to update the `schema.prisma` file and run `npx prisma migrate dev --name MIGRATION_NAME` and it will automatically update your prisma client

## Open prisma dashboard
Run the command `npx prisma studio`

## Endpoints

- Populate Shop and EmployeeType tables
```javascript
const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

prisma.shop.create({
  data: {
    name: 'shop1',
    address: 'address',
    telephone: '12345',
  }
})
.then(() => {})

prisma.employeeType.create({
  data: {
    name: 'type1',
    salary: '100',
  }
})
.then(() => {})
```

- Create an Employee
```bash
curl --location --request POST 'http://localhost:3000/employee' \
--header 'Content-Type: application/json' \
--data-raw '{"name":"Employee4","telephone":"123","address":"Address2","typeId":1,"shopId":1}'
```

- Get all
```bash
curl --location --request GET 'http://localhost:3000/employee'
```

- Get by ID
```bash
curl --location --request GET 'http://localhost:3000/employee/:id'
```

- Delete
```bash
curl --location --request DELETE 'http://localhost:3000/employee/:id'
```

- Update
```bash
curl --location --request PATCH 'http://localhost:3000/employee/:id' \
--header 'Content-Type: application/json' \
--data-raw '{ "telephone": "1111" }'
```