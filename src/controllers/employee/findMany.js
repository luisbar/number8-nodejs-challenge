const TraceError = require('trace-error')
const datasource = require('../../services/datasource')

const findMany = async (req, res, next) => {
  try {
    res.send(await datasource.findMany('employee', undefined, { type: true, shop: true }))
  } catch (error) {
    next(new TraceError('employee:findMany', error))
  }
}

module.exports = findMany