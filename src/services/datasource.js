const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()
const TraceError = require('trace-error')

const findMany = async (entity, where = {}, include = {}) => {
  try {
    return await prisma[entity].findMany({
      where,
      include,
    })
  } catch (error) {
    throw new TraceError('datasource:findMany', error)
  }
}

const create = async (entity, data = {}, include = {}) => {
  try {
    return await prisma[entity].create({
      data,
      include,
    })
  } catch (error) {
    throw new TraceError('datasource:data', error)
  }
}

const findOne = async (entity, where = {}, include = {}) => {
  try {
    return await prisma[entity].findUnique({
      where,
      include,
    })
  } catch (error) {
    throw new TraceError('datasource:findOne', error)
  }
}

const remove = async (entity, where = {}, include = {}) => {
  try {
    return await prisma[entity].delete({
      where,
      include,
    })
  } catch (error) {
    throw new TraceError('datasource:remove', error)
  }
}

const update = async (entity, where = {}, data = {}, include = {}) => {
  try {
    return await prisma[entity].update({
      where,
      data,
      include,
    })
  } catch (error) {
    throw new TraceError('datasource:update', error)
  }
}

module.exports = {
  findMany,
  create,
  findOne,
  remove,
  update,
}