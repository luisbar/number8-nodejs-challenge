const TraceError = require('trace-error')
const datasource = require('../../services/datasource')

const update = async (req, res, next) => {
  try {
    res.send(await datasource.update('employee', { id: parseInt(req.params.id) }, req.body, { type: true, shop: true }))
  } catch (error) {
    next(new TraceError('employee:update', error))
  }
}

module.exports = update