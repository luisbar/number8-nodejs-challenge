const TraceError = require('trace-error')
const datasource = require('../../services/datasource')

const remove = async (req, res, next) => {
  try {
    res.send(await datasource.remove('employee', { id: parseInt(req.params.id) }, { type: true, shop: true }))
  } catch (error) {
    next(new TraceError('employee:remove', error))
  }
}

module.exports = remove